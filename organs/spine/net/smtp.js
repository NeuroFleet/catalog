var SMTPServer = require('smtp-server');

exports.ports = {
    plain:  60025,
    secuee: 60465,
};

exports.callbacks = {
    banner: 'Welcome to My Awesome SMTP Server',
    login: function(auth, session, callback){
        var username = 'testuser';
        var password = 'testpass';

        // check username and password
        if (auth.username === username &&
            (
                auth.method === 'CRAM-MD5' ?
                auth.validatePassword(password) : // if cram-md5, validate challenge response
                auth.password === password // for other methods match plaintext passwords
            )
        ) {
            return callback(null, {
                user: 'userdata' // value could be an user id, or an user object etc. This value can be accessed from session.user afterwards
            });
        }

        return callback(new Error('Authentication failed'));
    },
    connect: function(session, callback){
        if (session.remoteAddress === '127.0.0.1') {
            return callback(new Error('No connections from localhost allowed'));
        }
        return callback(); // Accept the connection 
    },
    receive: function (stream, session, callback) {
        stream.pipe(process.stdout);
        stream.on('end', function () {
            var err;
            if (stream.sizeExceeded) {
                err = new Error('Error: message exceeds fixed maximum message size 10 MB');
                err.responseCode = 552;
                return callback(err);
            }
            callback(null, 'Message queued as abcdef'); // accept the message once the stream is ended
        });
    },
    message: function(address, session, callback){
        if (/^deny/i.test(address.address)) {
            return callback(new Error('Not accepted'));
        }
        callback();
    },
    recipient: function (address, session, callback) {
        var err;

        if (/^deny/i.test(address.address)) {
            return callback(new Error('Not accepted'));
        }

        // Reject messages larger than 100 bytes to an over-quota user
        if (address.address.toLowerCase() === 'almost-full@example.com' && Number(session.envelope.mailFrom.args.SIZE) > 100) {
            err = new Error('Insufficient channel storage: ' + address.address);
            err.responseCode = 452;
            return callback(err);
        }

        callback();
    },
    error: function (err) {
        console.log('Error occurred');
        console.log(err);
    },
};

exports.events = {
    init: function (config) {
        exports.serv = {
            plain: new SMTPServer({
                secure: false,
                logger: true,
                useXClient: true,
                useXForward: true,
                hidePIPELINING: true,
                authMethods: ['PLAIN', 'LOGIN', 'CRAM-MD5'],
                disabledCommands: ['AUTH', 'STARTTLS'],
                size: 10 * 1024 * 1024,
                banner: exports.callbacks.banner,
                onAuth: exports.callbacks.login,
                onConnect: exports.callbacks.connect,
                onMailFrom: exports.callbacks.message,
                onRcptTo: exports.callbacks.recipient,
                onData: exports.callbacks.receive,
            }),
        };

        exports.serv.plain.on('error', exports.callbacks.error);

        if (exports.config.ssl) {
            exports.serv.secure = new SMTPServer({
                key: exports.config.ssl.key,
                cert: exports.config.ssl.cert,
                secure: true,
                logger: true,
                useXClient: true,
                useXForward: true,
                hidePIPELINING: true,
                authMethods: ['PLAIN', 'LOGIN', 'CRAM-MD5'],
                disabledCommands: ['AUTH', 'STARTTLS'],
                size: 10 * 1024 * 1024,
                banner: exports.callbacks.banner,
                onAuth: exports.callbacks.login,
                onConnect: exports.callbacks.connect,
                onMailFrom: exports.callbacks.message,
                onRcptTo: exports.callbacks.recipient,
                onData: exports.callbacks.receive,
            });

            exports.serv.secure.on('error', exports.callbacks.error);

            exports.serv.secure.listen(exports.ports.secure, exports.config.listen_addr, function () {
                console.log("SMTP SSL server running on port '"+exports.config.ports.SMTP+"' ...")
            });
        }
    },
    open: function (session) {
        /*
        // Route to any global ip
        proxy.register("optimalbits.com", "http://167.23.42.67:8000");
        //*/
    },
    close: function (reason, details) {
        //callbacks.IO.finish(reason, details);
    },
};

exports.program = function (session) {
    exports.serv.plain.listen(exports.ports.plain, '0.0.0.0', function () {
        console.log("SMTP server running on port '"+exports.ports.plain+"' ...")
    });

    if (exports.serv.secure!=null) {
        exports.serv.secure.listen(exports.ports.secure, '0.0.0.0', function () {
            console.log("SMTP server with SSL running on port '"+exports.ports.secure+"' ...")
        });
    }
};

exports.methods = {
    /*
    'reactor.serve.http.route': function (target, source) {
        exports.serv.register("optimalbits.com", "http://167.23.42.67:8000");
    },
    'reactor.serve.http.contain': function (args) {
        // exports.docker.register("example.com", 'company/myimage:latest');
    },
    //*/
};

exports.topics = {
    /*
    '<sense>.image': function (args) {
    },
    //*/
};

