#!/bin/bash

#alias reactor='python -m reactor.shell.hub '
#alias psyhub='python -m reactor.shell.psy '
#alias cyborg='python -m reactor.shell.bio '

alias reactor='python -m reactor.shell '

#*******************************************************************************

cycle () {
    git submodule foreach git $*

    git $*
}

deming () {
    for pth in `echo $APP_ROOT{,/{catalog,reactor}} $APP_HOME` ; do
        cd $pth

        cycle $*
    done

    for key in `echo $APPLETs` ; do
        cd $APP_ROOT/nucleon/$key

        cycle $*
    done

    cd $APP_ROOT
}

#*******************************************************************************

ensure_repo () {
    if [[ ! -d $3 ]] ; then
        git clone $4 $3 --recursive
    else
        echo $1" '$2' already exists at : "$3
    fi
}

ensure_all () {
    for key in `echo $APPLETs` ; do
        ensure_repo 'Application' $key $APP_ROOT/nucleon/$key https://bitbucket.org/NeuroFleet/$key.git
    done

    for key in `echo $PROGRAMs` ; do
        ensure_repo 'Program' $key $APP_ROOT/programs/$key https://bitbucket.org/NeuroFleet/$key.git
    done
}

upgrade_all () {
    deming pull -u origin master
}

commit_all () {
    deming add --all

    deming commit -a -m Summarizing...
}

sync_all () {
    deming pull -u origin master

    deming push -u origin master
}

dock_all () {
    if [[ ! -d /stack/data ]] ; then
        mkdir -p /stack/data/{business/{docs,drive,erp/addons},backends/{cache/redis,mongo,graph}}

        pip install docker-compose

        cd /stack
    fi

    cd $APP_ROOT

    docker-compose pull

    #docker-compose up
}

