#!/bin/bash

export PORT=8000
export FLASK_DEBUG=1
export HOSTNAME=`hostname`

#*******************************************************************************

export IDENTITY=cyborg
export PERSONNA=tayamino
export ORGANISM=tayaa

#*******************************************************************************

export APP_DISK=/media/theque/SysOp

export APP_HOME=$APP_ROOT/ROOT/connector/$PERSONNA
export APP_DATA=$APP_DISK/ROOT/console/$ORGANISM
export APP_ROOT=$APP_DISK/ROOT/devops/neuro/hub

export APP_DIRS=$APP_ROOT/storage/$PERSONNA
export APP_EXEC=$APP_ROOT/program

if [[ ! -d $APP_HOME ]] ; then
    echo git clone https://bitbucket.org/NeuroFleet/$PERSONNA.git $APP_HOME --recursive
fi

#*******************************************************************************

export APP_TEMP=$APP_ROOT/temp
export APP_FILE=$APP_ROOT/files

export APP_PROG=$APP_HOME/progs
export APP_SBIN=$APP_HOME/tools
export APP_CODE=$APP_HOME/codes

export APP_WORK=$APP_HOME/works
export APP_SITE=$APP_HOME/sites

#*******************************************************************************

for key in catalog reactor ; do
    if [[ ! -d $APP_ROOT/$key ]] ; then
        echo git clone https://bitbucket.org/NeuroFleet/$key.git $APP_ROOT/$key --recursive
    fi
done

if [[ ! -d $APP_ROOT/nucleon ]] ; then
    mkdir -p $APP_ROOT/nucleon
fi

if [[ ! -f $APP_ROOT/nucleon/__init__.py ]] ; then
    touch $APP_ROOT/nucleon/__init__.py
fi

#*******************************************************************************

source $APP_ROOT/catalog/library.sh

if [[ -f $APP_HOME/environ.sh ]] ; then
    source $APP_HOME/environ.sh
else
    export LANGUAGE=en-US
    export APPLETs="connector console"
fi

for key in oauth token ; do
    target=~/.creds/$key.sh

    if [[ -f $target ]] ; then
        source $target
    fi
done

#*******************************************************************************

cat {reactor{,/*,/contrib/*},nucleon/*}/requirements.txt | grep -e "^$" -v >requirements.txt

source venv/bin/activate

export PATH="$PATH:$APP_ROOT/node_modules/.bin/:$APP_SBIN:$APP_FILE/scripts:$APP_TEMP/scripts"

export PYTHONPATH="$APP_HOME/codes:$APP_ROOT"

#*******************************************************************************

export NLTK_DATA=$APP_DIRS/corpora/nltk

export DJANGO_SETTINGS_MODULE="reactor.webapp.settings"

#*******************************************************************************

for pth in `echo $NLTK_DATA $APP_PROG $APP_SBIN $APP_CODE $APP_SITE $APP_WORK $APP_EXEC/{red,s3,ldf} $APP_TEMP/scripts` ; do
    if [[ ! -d $pth ]] ; then
        echo mkdir -p $pth
    fi
done

